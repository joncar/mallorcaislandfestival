<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function ajustes(){
            $crud = $this->crud_function('','');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function categorias(){
            $crud = $this->crud_function('','');
            $crud->set_field_upload('foto','img/fotos_categorias');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function productos(){
            $crud = $this->crud_function('','');
            $crud->unset_columns('categorias_id');
            $crud->set_field_upload('foto','img/fotos_productos');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function notificaciones(){
            $crud = $this->crud_function('','');
            $crud->set_field_upload('foto','img/fotos_productos');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function provincias(){
            $crud = $this->crud_function('','');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function ventas($x = ''){            
            $crud = $this->crud_function('','');
            $crud->set_relation('user_id','user','{nombre} {apellido_paterno}');            
            $crud->columns('id','user_id','productos','precio','cantidad','fecha_compra','procesado');
            $crud->display_as('id','Nro. Compra')
                 ->display_as('user_id','Usuario');
            
            if(is_numeric($x)){
                $crud->where('user_id',$x);
            }
            
            $crud->callback_column('se8701ad4',function($val,$row){
                return '<a href="'.base_url('admin/ventas/'.$row->user_id).'">'.$val.'</a>';
            });
            $crud->callback_column('procesado',function($val){
                switch($val){
                    case '-1': return '<span class="label label-danger">No procesado</span>'; break;
                    case '1': return '<span class="label label-default">Por procesar</span>'; break;
                    case '2': return '<span class="label label-success">Procesado</span>'; break;
                }
            });
            $crud->callback_after_delete(function($primary){
                get_instance()->db->delete('ventas_detalles',array('ventas_id'=>$primary));
            });
            $crud->field_type('procesado','dropdown',array(-1=>'No Procesado',1=>'Por procesar',2=>'Procesado'));
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
